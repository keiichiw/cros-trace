// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#include "tool-perfetto.h"

#include <fcntl.h>
#include <iostream>
#include <iterator>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <string>
#include <sys/file.h>
#include <unistd.h>

#include "tool-timesync.h"

enum perfetto_action {
    ACTION_INVALID,
    ACTION_START,
    ACTION_STOP,
    ACTION_WAIT,
    ACTION_KILL,
};

struct tool_perfetto {
    bool debug;
    const char *pid_file;
    enum perfetto_action action;

    const char *exec_path;
    const char *output_path;
    bool background;
    bool timesync;
    bool txt;
};

static const int perfetto_start_signum = SIGTERM;
static pid_t perfetto_start_child_pid = -1;

static bool
is_pid_alive(pid_t pid)
{
    const int ret = kill(pid, 0);
    return ret == 0 || errno_again();
}

static bool
poll_stdout_err(int timeout_ms)
{
    struct pollfd pfd = {
        .fd = STDOUT_FILENO,
        .events = POLLERR,
    };
    const int ret = poll(&pfd, 1, timeout_ms);
    if (ret < 0)
        return true;

    return ret > 0 ? (pfd.revents & (POLLERR | POLLHUP)) : false;
}

static int
perfetto_wait(struct tool_perfetto *perfetto)
{
    const pid_t pid = tool_read_pid_file(perfetto->pid_file);
    if (pid < 0) {
        tool_err("failed to get pid from %s", perfetto->pid_file);
        return 1;
    } else if (pid == 0) {
        tool_info("no daemon running");
        return 0;
    }

    if (perfetto->action == ACTION_STOP || perfetto->action == ACTION_KILL) {
        kill(pid, perfetto_start_signum);
        tool_info("killed pid %d", pid);
    }

    if (perfetto->action == ACTION_STOP || perfetto->action == ACTION_WAIT) {
        tool_info("waiting for pid %d...", pid);
        while (is_pid_alive(pid)) {
            /* we have to poll because we are not signaled when over ssh/adb */
            if (poll_stdout_err(300))
                return 1;
        }
    }

    return 0;
}

static void
perfetto_start_signal_handler(int signum)
{
    assert(perfetto_start_child_pid > 0);
    kill(perfetto_start_child_pid, signum);
}

static void
perfetto_start_install_signal_handler(pid_t child_pid)
{
    perfetto_start_child_pid = child_pid;

    struct sigaction sa = {};
    sa.sa_handler = perfetto_start_signal_handler;
    sigaction(perfetto_start_signum, &sa, NULL);
}

static void
perfetto_start_mask_signal(bool on)
{
    const int how = on ? SIG_BLOCK : SIG_UNBLOCK;

    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, perfetto_start_signum);
    pthread_sigmask(how, &sigset, NULL);
}

static int
perfetto_start_run(struct tool_perfetto *perfetto,
                   const char *config_data,
                   size_t config_size)
{
    int stdin_fds[2];
    if (pipe(stdin_fds)) {
        tool_err("failed to create stdin pipe");
        return -1;
    }

    perfetto_start_mask_signal(true);

    const pid_t pid = fork();
    if (pid < 0) {
        tool_err("failed to fork");
        close(stdin_fds[0]);
        close(stdin_fds[1]);
        return -1;
    }

    /* parent */
    if (pid > 0) {
        perfetto_start_install_signal_handler(pid);
        perfetto_start_mask_signal(false);

        close(stdin_fds[0]);
        const ssize_t w = tool_write(stdin_fds[1], config_data, config_size);
        close(stdin_fds[1]);

        if (w != (ssize_t)config_size) {
            tool_err("failed to write config data");
            kill(pid, perfetto_start_signum);
            tool_waitpid(pid);
            return -1;
        }

        struct timesync_thread timesync;
        int ret;

        if (perfetto->timesync) {
            tool_info("creating timesync thread...");
            ret = timesync_thread_create(&timesync);
            if (ret) {
                tool_err("failed to create timesync thread");
                kill(pid, perfetto_start_signum);
                tool_waitpid(pid);
                return ret;
            }
        }

        ret = tool_waitpid(pid);
        tool_info("perfetto exited");

        if (perfetto->timesync)
            timesync_thread_join(&timesync);

        return ret;
    }

    /* child */
    perfetto_start_mask_signal(false);

    close(stdin_fds[1]);

    if (stdin_fds[0] != STDIN_FILENO) {
        if (dup2(stdin_fds[0], STDIN_FILENO) < 0) {
            tool_err("failed to dup to stdin");
            close(stdin_fds[0]);
            exit(1);
        }
        close(stdin_fds[0]);
    }

    tool_info("executing perfetto...");
    const char *perfetto_argv[] = {
        perfetto->exec_path,
        "-c",
        "-",
        "-o",
        perfetto->output_path,
        perfetto->txt ? "--txt" : NULL,
        NULL,
    };
    const int ret = execvp(perfetto_argv[0], (char **)perfetto_argv);
    tool_err("execvp failed with %d", ret);
    exit(1);
}

static std::string
perfetto_start_read_stdin()
{
    std::istreambuf_iterator<char> begin(std::cin), end;
    std::string str(begin, end);
    return str;
}

static int
perfetto_start(struct tool_perfetto *perfetto)
{
    std::string config = perfetto_start_read_stdin();

    int pid_file_fd;
    if (perfetto->background) {
        pid_file_fd = tool_daemon(!perfetto->debug, perfetto->pid_file);
        if (pid_file_fd < 0) {
            tool_err("failed to daemonize");
            return 1;
        }
    } else {
        pid_file_fd = tool_create_pid_file(perfetto->pid_file, getpid());
        if (pid_file_fd < 0) {
            tool_err("failed to create pid file %s", perfetto->pid_file);
            return 1;
        }
    }

    const int ret = perfetto_start_run(perfetto, config.c_str(), config.size());

    unlink(perfetto->pid_file);
    close(pid_file_fd);

    return ret ? 1 : 0;
}

static void
perfetto_parse_options(struct tool_perfetto *perfetto, int argc, char **argv)
{
    for (int i = 0; i < argc; i++) {
        if (!strcmp(argv[i], "--debug"))
            perfetto->debug = true;
        else if (!strcmp(argv[i], "--pid-file") && i + 1 < argc)
            perfetto->pid_file = argv[i + 1];
        else if (!strcmp(argv[i], "--start"))
            perfetto->action = ACTION_START;
        else if (!strcmp(argv[i], "--stop"))
            perfetto->action = ACTION_STOP;
        else if (!strcmp(argv[i], "--wait"))
            perfetto->action = ACTION_WAIT;
        else if (!strcmp(argv[i], "--kill"))
            perfetto->action = ACTION_KILL;
        else if (!strcmp(argv[i], "--exec-path") && i + 1 < argc)
            perfetto->exec_path = argv[i + 1];
        else if (!strcmp(argv[i], "--output-path") && i + 1 < argc)
            perfetto->output_path = argv[i + 1];
        else if (!strcmp(argv[i], "--background"))
            perfetto->background = true;
        else if (!strcmp(argv[i], "--timesync"))
            perfetto->timesync = true;
        else if (!strcmp(argv[i], "--txt"))
            perfetto->txt = true;
    }

    if (!perfetto->pid_file || perfetto->action == ACTION_INVALID) {
        tool_err(
            "--pid-file and one of --start, --stop, --wait, or --kill must be "
            "specified");
        exit(1);
    }

    if (perfetto->action == ACTION_START) {
        if (!perfetto->exec_path || !perfetto->output_path) {
            tool_err("--exec-path and --output-path must be specified");
            exit(1);
        }
    }
}

int
tool_perfetto_main(int argc, char **argv)
{
    struct tool_perfetto perfetto = {};
    perfetto_parse_options(&perfetto, argc, argv);

    if (perfetto.debug)
        tool_dbg_enable();

    switch (perfetto.action) {
    case ACTION_START:
        return perfetto_start(&perfetto);
    case ACTION_STOP:
    case ACTION_WAIT:
    case ACTION_KILL:
        return perfetto_wait(&perfetto);
    default:
        unreachable("bad action");
    }
}
