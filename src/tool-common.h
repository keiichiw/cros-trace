// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#pragma once

#include <atomic>
#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>

#ifndef __has_attribute
#define __has_attribute(x) 0
#endif

#ifndef __has_builtin
#define __has_builtin(x) 0
#endif

#if __has_attribute(format)
#define PRINTFLIKE(f, a) __attribute__((format(printf, f, a)))
#else
#define PRINTFLIKE(f, a)
#endif

#if __has_builtin(__builtin_unreachable)
#define unreachable(msg)                                                       \
    do {                                                                       \
        assert(!msg);                                                          \
        __builtin_unreachable();                                               \
    } while (false)
#else
#define unreachable(msg) assert(!msg);
#endif

#define TOOL_COMM "cros-trace-tool"
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static inline bool
errno_again()
{
    return errno == EINTR || errno == EAGAIN;
}

void
tool_dbg_enable();

void
tool_dbg(const char *format, ...) PRINTFLIKE(1, 2);

void
tool_info(const char *format, ...) PRINTFLIKE(1, 2);

void
tool_err(const char *format, ...) PRINTFLIKE(1, 2);

int
tool_unsupported_main(int argc, char **argv);

ssize_t
tool_read(int fd, void *buf, size_t count);

ssize_t
tool_write(int fd, const void *buf, size_t count);

int
tool_waitpid(pid_t pid);

int
tool_create_pid_file(const char *pid_file, pid_t pid);

pid_t
tool_read_pid_file(const char *pid_file);

int
tool_daemon(bool close_stdio, const char *pid_file);
