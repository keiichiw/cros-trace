// Copyright 2020 The Android Open Source Project
// SPDX-License-Identifier: Apache-2.0

#include <cstdint>

// Convenient declspec dllexport macro for android-emu-shared on Windows
#ifndef VPERFETTO_EXPORT
    #ifdef _MSC_VER
        #define VPERFETTO_EXPORT __declspec(dllexport)
    #else // _MSC_VER
        #define VPERFETTO_EXPORT __attribute__((visibility("default")))
    #endif // !_MSC_VER
#endif // !VPERFETTO_EXPORT

namespace vperfetto {

// An API to use offline to combine traces. The user can specify the guest/host trace files
// along with an optional argument for the guest clock boot time at start of tracing.
struct TraceCombineConfig {
    const char* guestFile;
    const char* hostFile;
    const char* combinedFile;

    // Whether or not to derive the guest clock boot time from the guest trace.
    // Less accurate than explicit specification.
    bool useGuestAbsoluteTime = false;
    // Guest time when tracing begins, to line up with host.
    uint64_t guestClockBootTimeNs;

    // Use a time diff instead of absolute time to line up.
    // Overriden by useSpecifiedGuestAbsoluteTime.
    bool useGuestTimeDiff = false;
    int64_t guestClockTimeDiffNs;

    // Use a tsc offset when deriving time sync between host and guest using rdtsc.
    int64_t guestTscOffset = 0;

    // Merge guest events into host space if true; otherwise merge host events into guest.
    bool mergeGuestIntoHost = false;

    // Simply display the two separate traces in one trace. Do not modify them in any way.
    bool addTraces;
};

// Reads config.guestFile
// Reads config.hostFile
// Writes config.combinedFile
void combineTraces(const TraceCombineConfig* config);

} // namespace vperfetto
