// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#include "tool-common.h"

#include <cstdarg>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <unistd.h>

static bool _tool_dbg_enable;

static void
tool_logv(FILE *file, const char *format, va_list ap)
{
    flockfile(file);

    vfprintf(file, format, ap);
    fprintf(file, "\n");

    funlockfile(file);
}

void
tool_dbg_enable()
{
    _tool_dbg_enable = true;
}

void
tool_dbg(const char *format, ...)
{
    if (!_tool_dbg_enable)
        return;

    va_list ap;
    va_start(ap, format);
    tool_logv(stdout, format, ap);
    va_end(ap);
}

void
tool_info(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    tool_logv(stdout, format, ap);
    va_end(ap);
}

void
tool_err(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    tool_logv(stderr, format, ap);
    va_end(ap);
}

int
tool_unsupported_main(int argc, char **argv)
{
    assert(argc);
    tool_err("%s is not supported", argv[0]);
    return 1;
}

ssize_t
tool_read(int fd, void *buf, size_t count)
{
    size_t remain = count;
    do {
        const ssize_t r = read(fd, buf, remain);
        if (r < 0) {
            if (errno_again())
                continue;
            return r;
        } else if (r == 0) {
            break;
        }

        buf = (char *)buf + r;
        remain -= r;
    } while (remain);

    return count - remain;
}

ssize_t
tool_write(int fd, const void *buf, size_t count)
{
    size_t remain = count;
    do {
        const ssize_t w = write(fd, buf, remain);
        if (w < 0) {
            if (errno_again())
                continue;
            return w;
        }

        buf = (const char *)buf + w;
        remain -= w;
    } while (remain);

    return count;
}

int
tool_waitpid(pid_t pid)
{
    int ret;
    do {
        ret = waitpid(pid, NULL, 0);
    } while (ret < 0 && errno_again());

    return ret;
}

/* To write to a pid file, we grab a exclusive lock, write the pid, and
 * downgrade to a shared lock for the life time of the daemon.  This makes
 * sure a second instance of the daemon will fail to grab the exclusive lock
 * and bail.
 */
int
tool_create_pid_file(const char *pid_file, pid_t pid)
{
    char buf[16];
    const int size = snprintf(buf, sizeof(buf), "%d", pid);

    const int fd = open(pid_file, O_CREAT | O_WRONLY | O_CLOEXEC, 0644);
    if (fd < 0)
        goto fail;

    if (flock(fd, LOCK_EX | LOCK_NB))
        goto fail;

    if (ftruncate(fd, 0))
        goto fail;

    if (tool_write(fd, buf, size) != size)
        goto fail;

    /* downgrade to shared lock */
    if (flock(fd, LOCK_SH))
        goto fail;

    return fd;

fail:
    if (fd >= 0)
        close(fd);
    return -1;
}

static const char *
read_line(int fd, char *buf, size_t size)
{
    const ssize_t r = tool_read(fd, buf, size);
    if (r < 0 || r >= (ssize_t)size)
        return NULL;

    buf[r] = '\0';
    char *p = strchr(buf, '\n');
    if (p)
        *p = '\0';
    return buf;
}

static pid_t
read_pid(int fd)
{
    char buf[16];
    const ssize_t r = tool_read(fd, buf, sizeof(buf));
    if (r < 0 || r >= (ssize_t)sizeof(buf))
        return -1;

    buf[r] = '\0';
    return atoi(buf);
}

static bool
validate_pid(pid_t pid)
{
    char path[64];
    snprintf(path, sizeof(path), "/proc/%d/comm", pid);

    const int fd = open(path, O_RDONLY);
    if (fd < 0)
        return false;

    const char *comm = read_line(fd, path, sizeof(path));
    close(fd);

    if (!comm)
        return false;

    return !strcmp(comm, TOOL_COMM);
}

/* To read from a pid file, we grab a shared lock, read the pid, and validate
 * it.
 *
 * We allow the pid file to be stale because the pid file may be on persistent
 * storage.  The validation is not perfect but should be good enough as long
 * as we always start the daemon first.
 */
pid_t
tool_read_pid_file(const char *pid_file)
{
    pid_t pid;
    int ret;

    const int fd = open(pid_file, O_RDONLY);
    if (fd < 0)
        return 0;

    ret = flock(fd, LOCK_SH);
    if (ret < 0)
        goto fail;

    pid = read_pid(fd);
    if (pid <= 0)
        goto fail;

    close(fd);

    /* the pid file might be stale */
    if (!validate_pid(pid)) {
        tool_dbg("ignored staled pid file %s", pid_file);
        return 0;
    }

    return pid;

fail:
    if (fd >= 0)
        close(fd);
    return -1;
}

static bool
redirect_stdio()
{
    const int null_fd = open("/dev/null", O_RDWR);
    if (null_fd < 0)
        return false;

    if (dup2(null_fd, STDIN_FILENO) < 0 || dup2(null_fd, STDOUT_FILENO) < 0 ||
        dup2(null_fd, STDERR_FILENO) < 0) {
        close(null_fd);
        return false;
    }

    if (null_fd != STDIN_FILENO && null_fd != STDOUT_FILENO &&
        null_fd != STDERR_FILENO)
        close(null_fd);

    return true;
}

int
tool_daemon(bool close_stdio, const char *pid_file)
{
    int pid_file_fd = -1;
    int ret;

    int wait_fds[2];
    if (pipe(wait_fds)) {
        tool_err("failed to create wait pipe");
        return -1;
    }

    pid_t pid = fork();
    if (pid < 0) {
        tool_err("failed to fork");
        close(wait_fds[0]);
        close(wait_fds[1]);
        return -1;
    }

    /* parent */
    if (pid > 0) {
        close(wait_fds[1]);

        /* wait for notification from child */
        const ssize_t r = tool_read(wait_fds[0], &ret, sizeof(ret));
        close(wait_fds[0]);

        if (r < (ssize_t)sizeof(ret))
            ret = -1;
        if (ret) {
            tool_err("child failed");
            tool_waitpid(pid);
            return ret;
        }

        tool_dbg("exiting parent process");
        /* parent can exit now */
        exit(0);
    }

    /* child */
    close(wait_fds[0]);

    chdir("/");

    const pid_t sid = setsid();
    if (sid == (pid_t)-1) {
        tool_err("failed to setsid()");
        goto fail;
    }

    if (close_stdio && !redirect_stdio()) {
        tool_err("failed to redirect stdin/stdout/stderr");
        goto fail;
    }

    if (pid_file) {
        tool_dbg("creating pid file %s", pid_file);
        pid_file_fd = tool_create_pid_file(pid_file, getpid());
        if (pid_file_fd < 0) {
            tool_err("failed to create pid file");
            goto fail;
        }
    }

    ret = 0;
    if (tool_write(wait_fds[1], &ret, sizeof(ret)) != (ssize_t)sizeof(ret)) {
        tool_err("failed to notify the parent");
        goto fail;
    }

    close(wait_fds[1]);

    /* child can continue now */
    return pid_file ? pid_file_fd : 0;

fail:
    if (pid_file_fd >= 0)
        close(pid_file_fd);
    close(wait_fds[1]);
    exit(1);
}
