# Copyright 2022 Google LLC
# SPDX-License-Identifier: MIT

files_tool = files(
  'tool-common.cpp',
  'tool-main.cpp',
  'tool-perfetto.cpp',
)
args_tool = []
deps_tool = [dep_threads]

if enable_tool_merge
  perfetto_trace_pb = custom_target(
    'perfetto_trace.pb',
    output: ['perfetto_trace.pb.cc', 'perfetto_trace.pb.h'],
    input: [perfetto_datadir / 'perfetto_trace.proto'],
    command: [prog_protoc,
      '--cpp_out=@OUTDIR@',
      '--proto_path=' + perfetto_datadir,
      '@INPUT@',
    ]
  )

  files_tool += ['tool-merge.cpp', 'vperfetto-sdk.cpp', perfetto_trace_pb]
  args_tool += ['-DTOOL_ENABLE_MERGE']
  deps_tool += [dep_protobuf]
endif

if enable_tool_timesync
files_tool += ['tool-timesync.cpp']
args_tool += ['-DTOOL_ENABLE_TIMESYNC']
deps_tool += [dep_perfetto, dep_rt, dep_threads]
endif

executable(
  'cros-trace-tool',
  files_tool,
  cpp_args: args_tool,
  dependencies: deps_tool,
  install: true,
)
