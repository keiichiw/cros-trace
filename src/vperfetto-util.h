// Copyright 2020 The Android Open Source Project
// SPDX-License-Identifier: Apache-2.0

#include <cstdint>

// Assumes that the difference is less than abs(INT64_MAX).
static inline int64_t getSignedDifference(uint64_t a, uint64_t b) {
    uint64_t absDiff = (a > b) ? a - b : b - a;
    absDiff = (absDiff < INT64_MAX) ? absDiff : INT64_MAX;
    return (a > b) ? (int64_t)absDiff : -(int64_t)absDiff;
}
