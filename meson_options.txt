# Copyright 2022 Google LLC
# SPDX-License-Identifier: MIT

option(
  'scripts',
  type: 'boolean',
  value: true,
  description : 'Install scripts and configs',
)

option(
  'tool-merge',
  type : 'boolean',
  value : true,
  description : 'Enable cros-trace-tool merge command',
)

option(
  'tool-timesync',
  type : 'boolean',
  value : true,
  description : 'Enable cros-trace-tool timesync command',
)
